#!/usr/bin/env python
# coding: utf-8

# In[4]:

# library_file

import numpy as np
import sklearn.model_selection as sms

# LRGradientDescent function:

def LRGradientDescent(learning_rate, data1, data2):
    b = []
    for i in range(0,data1.shape[1]):
        b.append(0)
    
    a = 0
    guess = a
    
    for i in range(0,data1.shape[0]):
        x = []
        for p in range(0,data1.shape[1]):
            x.append(0)
        for j in range(0,data1.shape[1]):
            x[j] = data1.iat[i,j]
            guess = guess + b[j]*x[j]
        
        error = guess - data2.iat[i]
        
        a = a + error*learning_rate
        
        for k in range(0,data1.shape[1]):
            b[k] = b[k] + (error*x[k])*learning_rate
            
    c = []
    for i in range(0,data1.shape[1]+1):
        c.append(0)
    
    c[0] = a
    for i in range(1, data1.shape[1]+1):
        c[i] = b[i-1]
    
    return c


# ComputationTime function:    

import time
def ComputationTime(learning_rate, data1, data2):
    
    time_start = time.perf_counter()
    b = []
    for i in range(0,data1.shape[1]):
        b.append(0)
    
    a = 0
    guess = a
    
    for i in range(0,data1.shape[0]):
        x = []
        for p in range(0,data1.shape[1]):
            x.append(0)
        for j in range(0,data1.shape[1]):
            x[j] = data1.iat[i,j]
            guess = guess + b[j]*x[j]
        
        error = guess - data2.iat[i]
        
        a = a + error*learning_rate
        
        for k in range(0,data1.shape[1]):
            b[k] = b[k] + (error*x[k])*learning_rate
            
    c = []
    for i in range(0,data1.shape[1]+1):
        c.append(0)
    
    c[0] = a
    for i in range(1, data1.shape[1]+1):
        c[i] = b[i-1]
    
    time_elapsed = (time.perf_counter() - time_start)
    
    return(time_elapsed)

# RSquared function:

def RSquared(learning_rate,k,data1,data2):
    kf = sms.KFold(n_splits = k, shuffle = True)
    scores = []
    for i in range(k):
        result = next(kf.split(data1), None)
        x_train = data1.iloc[result[0]]
        x_test = data1.iloc[result[1]]
        y_train = data2.iloc[result[0]]
        y_test = data2.iloc[result[1]]
        x_test.insert(0, "Ones", np.ones(x_test.shape[0])) # Creating a column to 1s in x_test data.
        sum = 0
        for j in range(0,x_test.shape[0]):
            sum = sum + y_test.iat[j]
        y_bar = sum/x_test.shape[0]
        ssr = 0
        for j in range(0,x_test.shape[0]):
            ssr = ssr + (np.dot(LRGradientDescent(learning_rate,x_train,y_train),x_test.iloc[j]) - y_bar)**2
        sst = 0
        for j in range(0,x_test.shape[0]):
            sst = sst + (y_test.iat[j] - y_bar)**2
        
        r_squared = 1-(ssr/sst)
            
        scores.append(r_squared)
    
    return scores, np.mean(scores)

# MSE function:

def MSE(learning_rate,k,data1,data2):
    kf = sms.KFold(n_splits = k, shuffle = True)
    scores = []
    for i in range(k):
        result = next(kf.split(data1), None)
        x_train = data1.iloc[result[0]]
        x_test = data1.iloc[result[1]]
        y_train = data2.iloc[result[0]]
        y_test = data2.iloc[result[1]]
        x_test.insert(0, "Ones", np.ones(x_test.shape[0])) # Creating a column to 1s in x_test data.
        sum = 0
        for j in range(0,x_test.shape[0]):
            sum = sum + (np.dot(LRGradientDescent(learning_rate,x_train,y_train),x_test.iloc[j]) - y_test.iat[j])**2
        mse = sum/x_test.shape[0]
        scores.append(mse)
    
    return scores, np.mean(scores) 

# MAE function:

def MAE(learning_rate,k,data1,data2):
    kf = sms.KFold(n_splits = k, shuffle = True)
    scores = []
    for i in range(k):
        result = next(kf.split(data1), None)
        x_train = data1.iloc[result[0]]
        x_test = data1.iloc[result[1]]
        y_train = data2.iloc[result[0]]
        y_test = data2.iloc[result[1]]
        x_test.insert(0, "Ones", np.ones(x_test.shape[0])) # Creating a column to 1s in x_test data.
        sum = 0
        for j in range(0,x_test.shape[0]):
            sum = sum + abs((np.dot(LRGradientDescent(learning_rate,x_train,y_train),x_test.iloc[j]) - y_test.iat[j]))
        mae = sum/x_test.shape[0]
        scores.append(mae)
    
    return scores, np.mean(scores)


    
    
    
    
    
    
    
    
    
    
    
    
    
    
