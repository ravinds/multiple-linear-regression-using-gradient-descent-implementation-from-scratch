# Multiple Linear Regression via Gradient Descent - Implementation from Scratch

**Summary:**

A gradient descent algorithm is implemented on a dataset containing multiple predictor variables and a single target variable.

The gist of this algorithm is expressed by the following mathematical equations,

Cost(b0, b1,...., bp) ~ (Error)^2

Error = y_guess - y_actual = [(b0 + b1x1 + ..... + bpxp) - y]

Partial derivate of Cost function with respect to b0 = 2.Error.(Partial derivative of Error wrt b0) = 2.Error.1 (By using chain rule)

Similarly, Partial derivative of Cost functuon wrt b1 = 2.Error.(Partial derivative of Error wrt b1) = 2.Error.x1

We find similar equations for b2,....bp.

The right hand side of above equations are directions in which the algorithm moves. The learning rate decides the speed of the movement. The learning rate is a hyper parameter here.
The values used for learning rate in my analysis are 0.001, 0.0005, 0.00025, and 0.000125.

Now, for i = 0, 1, 2.....p, we have bi = bi + (change in bi)

i.e., b0 = b0 + learning_rate.Error

      bi = bi + learning_rate.Error.xi for i = 1, 2, .......p.

We apply the for loop to update the values of bi's. We initialize all bi's to be 0. However, any initialization works.


**Results:**

I chose Computation time, R Squared, MSE and MAE as the metrics for comparison between my implementation and sklearn linear regression implementation (as a benchmark). I created
functions for all 4 metrics in my library file.

*ComputationTime*- 
My implementation performed better than sklearn, with computation time lower than sklearn across all different learning_rate. I believe this is because of finite number of linear equations 
involved in my LRGradientDescent function, which are quick to compute. Also, Gradient descent is generally a quicker way for fitting.

Among different learning_rate, the computation time generally didn't show any trend.

Note: I implemented 5-fold cross validation for R Squared, MSE and MAE.

*R Squared*-
My algorithm performed much better in terms of R Squared value, with higher values than that of sklearn. I believe this is because of the better fit of my implementation to the data than
the sklearn.

Among different learning_rate, there was no specific trend observed. The R Squared values were all close to 95% for each learning_rate.

*MSE*-
The sklearn performed better than my implementation. However, the difference in the MSE values was not significant, infact for some learning_rate the MSE value was found to be lower than that of
sklearn. The results here were slightly ambiguous.

Among different learning_rate, the MSE value increased from learning_rate = 0.001 to learning_rate = 0.0005, however then MSE decreased as the learning_rate was lowered down further.

*MAE*-
The sklearn performed better than my implementation across all learning_rate. However, again the difference in the MAE values was not very significant. I believe sklearn performed slightly
better than my implementation here because it might be true that sklearn algorithm is more sensitive to MAE values.

Among different learning_rate, the MAE value increased as the learning_rate decreased from 0.001 to 0.000125.

**Reflection:**

*Useful takeaways*-

1.) I could create my own code and library for regression algorithm and didn't depend on the sklearn library to fit the model to the data.
2.) I could understand the inner mechanism of the gradient descent algorithm.
3.) I could understand the effect of hyperparameter (for example, learning_rate) on the performance of the algorithm.
4.) I came across various Python libraries and made use of them in my own library.

*Things that surprised me*-

1.) The amount of efforts that are spent in order to create a library for even a simple algorithm like gradient descent. It also made me appreciate the overall coding perseverance.

*Things that caused difficulties and challenged me*-

1.) I couldn't see any definite/sharp trend in the change of metric values on changing hyperparameter's values.
2.) The amount of efforts that are spent in writing a code that is generic and works for any arbitrary dataset and learning_rate and not just a specific dataset and fixed learning_rate.
